package fr.iut.ocr;

import fr.iut.ocr.features.Feature;
import fr.iut.ocr.features.exception.NotComparableException;
import ij.process.ImageProcessor;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sydpy on 3/24/17.
 */
public class Analysis {

    private List<Feature> features;

    private ImageProcessor imageProcessor;

    public Analysis(ImageProcessor imageProc, List< Class<? extends Feature> > caracteristics) {
        this.imageProcessor = imageProc;
        run(caracteristics);
    }

    private void run(List<Class<? extends Feature>> caracteristicsClasses) {

        features = new ArrayList<>();

        for (Class<? extends Feature> c : caracteristicsClasses) {

            Constructor<?> ctor;

            try {
                ctor = c.getDeclaredConstructor(ImageProcessor.class);

                Feature carac = (Feature) ctor.newInstance(imageProcessor);

                features.add(carac);

            } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
                e.printStackTrace(System.err);
            }

        }
    }

    public List<Feature> getFeatures() {
        return features;
    }

    public double computeError(Analysis analysis) throws NotComparableException {

        if(analysis.getFeatures().size() != features.size()) {
            throw new NotComparableException("The two analysis don't have the same number of fr.iut.ocr.features !");
        }

        double error = 0.;

        for (int i = 0; i < features.size(); i++) {

            if(features.get(i).getClass() != analysis.getFeatures().get(i).getClass()) {
                throw new NotComparableException("The two analysis don't have the same types of fr.iut.ocr.features !");
            }

            double comparison = features.get(i).compareWith(analysis.getFeatures().get(i));

            error += Math.pow(comparison, 2);
        }


        return Math.sqrt(error);
    }

}
