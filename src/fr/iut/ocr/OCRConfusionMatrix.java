package fr.iut.ocr;

import fr.iut.ocr.features.*;
import fr.iut.ocr.features.exception.NotComparableException;
import ij.ImagePlus;
import ij.gui.GenericDialog;
import ij.io.DirectoryChooser;
import ij.measure.ResultsTable;
import ij.plugin.PlugIn;
import ij.process.ImageConverter;
import ij.process.ImageProcessor;

import java.io.File;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Sydpy on 3/31/17.
 */
public class OCRConfusionMatrix implements PlugIn {


    /**
     * Caracteristics to use during comparison.
     */
    private List< Class<? extends Feature> > FEATURES = Arrays.asList(
            GrayAverageZoningFeature.class,
            HorizontalProfileFeature.class,
            VerticalProfileFeature.class,
            IsoperimetricFeature.class
    );

    private Map< String, Map<String, Integer> > confusionMap = new HashMap<>();

    private Map<Analysis, File> imageProcToFile = new HashMap<>();

    @Override
    public void run(String s) {

        //Dialog to choose dataset
        GenericDialog gd = new GenericDialog("New Image");
        gd.setTitle("Data set chooser");
        gd.addMessage("Choose the folder containing your set of labeled images.\nThe filenames of your images must match \"<label>_<index>.png\"");

        gd.showDialog();
        if (gd.wasCanceled())
            return;

        DirectoryChooser directoryChooser = new DirectoryChooser("Select data set folder");
        String directory = directoryChooser.getDirectory();

        if(directory == null || directory.isEmpty())
            return;

        //Files from dataset
        List<File> files = Arrays.asList(listFiles(directory));

        Pattern wellNamed = Pattern.compile(".*_.*\\.png");

        for (File file : files) {
            if(!file.isHidden() && wellNamed.matcher(file.getName()).matches()) {

                ImageProcessor imgProc = preprocessImage(file);
                Analysis analysis = new Analysis(imgProc, FEATURES);
                imageProcToFile.put(analysis, file);

            }
        }

        for (Analysis mysteryImage : imageProcToFile.keySet()) {

            Analysis closestImage = findClosestImageFile(mysteryImage, new ArrayList<>(imageProcToFile.keySet()));
            File closestImageFile = imageProcToFile.get(closestImage);

            Pattern pa = Pattern.compile("-?\\d+");

            Matcher ma = pa.matcher(closestImageFile.getName());
            ma.find();
            String closest = ma.group();

            ma = pa.matcher(imageProcToFile.get(mysteryImage).getName());
            ma.find();
            String actual = ma.group();

            if(confusionMap.get(actual) == null) {

                Map<String, Integer> map = new HashMap<>();
                map.put(closest, 1);
                confusionMap.put(actual, map);

            } else if(confusionMap.get(actual).get(closest) == null) {

                confusionMap.get(actual).put(closest, 1);

            } else {

                Integer currentval = confusionMap.get(actual).get(closest);
                confusionMap.get(actual).put(closest, currentval + 1);

            }
        }
        for (String label : confusionMap.keySet()) {
            for (String s1 : confusionMap.keySet()) {
                confusionMap.get(label).putIfAbsent(s1, 0);
            }
        }

        //Display results

        ResultsTable resultsTable = new ResultsTable();
        resultsTable.showRowNumbers(false);
        resultsTable.incrementCounter();
        int size = confusionMap.size();

        for(int i = 0; i < size; i++) {
            resultsTable.setLabel(Integer.toString(i),i);

            String label = (String) confusionMap.keySet().toArray()[i];

            resultsTable.setLabel(label, i);

            if(i < size - 1) {
                resultsTable.incrementCounter();
            }

            int count = 0;
            for (Map.Entry<String, Integer> entry : confusionMap.get(label).entrySet()) {
                resultsTable.setValue(entry.getKey(), i, entry.getValue());
                count += entry.getValue();
            }
            double ratio = ((double) confusionMap.get(label).get(label)) / ((double) count);

            resultsTable.setValue("Success", i, Double.toString(ratio * 100) + "%");

        }

        resultsTable.show("Confusion Matrix");
    }

    /**
     * @param image image to compare.
     * @param dataSet list of images in which you want to search the most similar
     * @return the image file of the closest image.
     */
    private Analysis findClosestImageFile(Analysis image, List<Analysis> dataSet) {

        ArrayList<Analysis> dataSetCpy = new ArrayList(dataSet);
        dataSetCpy.remove(image);

        double minError = Double.MAX_VALUE;
        Analysis closestImage = null;

        for (Analysis img : dataSetCpy) {

            double error = 0;
            try {
                error = image.computeError(img);
            } catch (NotComparableException e) {
                e.printStackTrace();
            }
            if (error < minError) {
                minError = error;
                closestImage = img;
            }
        }

        return closestImage;
    }

    private File[] listFiles(String dirPath) {
        File dir = new File(dirPath);
        System.out.println("Listing files from : " + dir.getAbsolutePath());
        return dir.listFiles();
    }

    /**
     * @param file to preprocess.
     * @return the ImageProcessor ready to be analysed.
     */
    private ImageProcessor preprocessImage(File file) {

        ImagePlus imp = new ImagePlus(file.getAbsolutePath());

        new ImageConverter(imp).convertToGray8();

        ImageProcessor ip = imp.getProcessor();

        binarize(ip, 128);

        ip = ip.resize(50,50);

        return ip;
    }

    /**
     * @param ip ImageProcessor to binarize.
     * @param threshold threshold for the binarization.
     */
    private void binarize(ImageProcessor ip, int threshold) {
        int width = ip.getWidth();
        int height = ip.getHeight();

        byte[] pixels = (byte[]) ip.getPixels();

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if ((pixels[i * width + j] & 0xFF) < threshold) {
                    pixels[i * width + j] = (byte) 0;
                } else {
                    pixels[i * width + j] = (byte) 255;
                }
            }
        }
    }

}
