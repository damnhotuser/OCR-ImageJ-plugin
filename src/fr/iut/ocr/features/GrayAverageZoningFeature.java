package fr.iut.ocr.features;

import ij.process.ImageProcessor;

/**
 * Created by Sydpy on 3/24/17.
 *
 * Divide the image on sections and compute the gray average for each section separately.
 */
public class GrayAverageZoningFeature extends Feature {

    //needs to be a square
    private static int SECTION_COUNT = 9;

    private double[] values;

    public GrayAverageZoningFeature(ImageProcessor imageProcessor) {
        super(imageProcessor);

        computeValues();
    }

    public void computeValues() {

        ImageProcessor[] imageProcessors = new ImageProcessor[SECTION_COUNT];
        values = new double[SECTION_COUNT];

        int width = imageProcessor.getWidth();
        int height = imageProcessor.getHeight();

        int rowCount = (int) Math.sqrt(SECTION_COUNT);
        int colCount = rowCount;

        int count = 0;
        for (int i = 0; i < rowCount; i++) {
            for(int j = 0; j < colCount; j++) {

                imageProcessor.setRoi(
                        width/colCount * i,
                        height/rowCount * j,
                        width/colCount,
                        height/rowCount
                );

                imageProcessors[count] = imageProcessor.crop();
                values[count] = computeValues(imageProcessors[count]);
                count++;
            }
        }
    }

    private double computeValues(ImageProcessor imageProcessor) {
        byte[] pixels = (byte[]) imageProcessor.getPixels();

        int sum = 0;

        for (byte pixel : pixels) {
            sum += pixel & 0xFF;
        }

        return ((double) sum) / ((double) pixels.length);
    }

    @Override
    public double compareWith(Feature feature) {

        double comparison = 0.;

        for (int i = 0; i < SECTION_COUNT; i++) {
            comparison += Math.pow(values[i] - ((GrayAverageZoningFeature) feature).values[i], 2);
        }

        return Math.sqrt(comparison);
    }
}