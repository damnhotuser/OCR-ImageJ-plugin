package fr.iut.ocr.features;

import ij.process.ImageProcessor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sydpy on 4/3/17.
 */
public class HorizontalProfileFeature extends ProfileFeature {

    public HorizontalProfileFeature(ImageProcessor imageProcessor) {
        super(imageProcessor);
    }

    @Override
    protected List<Integer> computeProfile(ImageProcessor ip) {
        List<Integer> profile = new ArrayList<>();

        int width = ip.getWidth();
        int height = ip.getHeight();

        byte[] pixels = (byte[]) ip.getPixels();

        for (int i = 0; i < height; i++) {

            int count = 0;

            for (int j = 0; j < width; j++) {
                if ((pixels[i * width + j] & 0xFF) == 0) {
                    count++;
                }
            }

            profile.add(count);
        }

        return profile;
    }

}
