package fr.iut.ocr.features;

import ij.process.ImageProcessor;

/**
 * Created by Sydpy on 3/24/17.
 */
public abstract class Feature {

    protected ImageProcessor imageProcessor;

    public Feature(ImageProcessor imageProcessor) {
        this.imageProcessor = imageProcessor;
    }

    public abstract double compareWith(Feature feature);

}
