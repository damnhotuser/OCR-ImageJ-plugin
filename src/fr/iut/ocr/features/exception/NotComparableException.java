package fr.iut.ocr.features.exception;

/**
 * Created by Sydpy on 3/24/17.
 */
public class NotComparableException extends Exception{

    public NotComparableException() {
        super();
    }

    public NotComparableException(String s) {
        super(s);
    }
}
