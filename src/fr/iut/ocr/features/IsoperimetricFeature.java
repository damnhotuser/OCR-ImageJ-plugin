package fr.iut.ocr.features;

import ij.process.ImageProcessor;

/**
 * Created by Sydpy on 4/4/17.
 */
public class IsoperimetricFeature extends Feature {

    private int value = 0;

    public IsoperimetricFeature(ImageProcessor imageProcessor) {
        super(imageProcessor);
        computeValue();
    }

    private void computeValue() {
        value = 0;
        byte[] pixels = (byte[]) imageProcessor.getPixels();

        for (int i = 0; i < pixels.length; i++) {

            if( (pixels[i] & 0xFF) == 0 && hasWhiteNeighbor(i)) {
                value++;
            }
        }
    }

    private boolean hasWhiteNeighbor(int pixelIndex) {

        int width = imageProcessor.getWidth();

        byte[] pixels = (byte[]) imageProcessor.getPixels();

        int fromX = pixelIndex%width == 0 ? 0 : -1;
        int toX = pixelIndex%width == width - 1 ? 0 : 1;

        int fromY = pixelIndex < width ? 0 : -1;
        int toY = pixelIndex > pixels.length - 1 - width ? 0 : 1;

        for(int i = fromX; i <= toX; i++) {
            for(int j = fromY; j <= toY; j++) {
                if( (pixels[pixelIndex + j*width + i] & 0xFF) == 255)
                    return true;
            }
        }

        return false;
    }

    @Override
    public double compareWith(Feature feature) {
        return value - ((IsoperimetricFeature) feature).value;
    }
}
