package fr.iut.ocr.features;

import ij.process.ImageProcessor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sydpy on 4/3/17.
 */
public abstract class ProfileFeature extends Feature {

    protected List<Integer> profile = new ArrayList<>();

    public ProfileFeature(ImageProcessor imageProcessor) {
        super(imageProcessor);
        profile = computeProfile(imageProcessor);
    }

    protected abstract List<Integer> computeProfile(ImageProcessor ip);

    @Override
    public double compareWith(Feature feature) {

        double error = 0.;

        for (int i = 0; i < profile.size(); i++) {
            error += Math.pow( profile.get(i) - ((ProfileFeature) feature).profile.get(i), 2);
        }

        return Math.sqrt(error);
    }

}
