# OCR-ImageJ-plugin
[https://sydpy.github.io/OCR-ImageJ-plugin/](https://sydpy.github.io/OCR-ImageJ-plugin/)
## Auteurs
* [Sylvain Dupouy](https://github.com/Sydpy/ "Github de Sylvain")
* [Maxime Dubourg](https://github.com/mdubourg001 "Github de Maxime")
## Description
Plugin ImageJ implémentant l'OCR (Optical Character Recognition)
## Instructions
1. Lancer le script _build.sh_ qui génère l'archive .jar du plugin et la copie dans le dossier  _~/.imagej/plugins_
2. (Re)Démarrer ImageJ ou bien cliquer sur _Refresh Menus_ dans le menu _Help_
3. Le plugin est maintenant présent dans le menu _Plugins_

## Résumé

### Enjeux
La finalité de ce plugin est la reconnaissance de caractères manuscrits. En d'autres termes, il doit associer un label à une image non labellisée d'un caractère.

### Attentes
La méthode de labellisation des images est la comparaison avec une base de données d'images déjà labellisées. Pour pouvoir visualiser l'efficacité de l'implémentation, le plugin _OCR Confusion Matrix_ génère une matrice de confusion en s'appuyant sur les images de la bse de données. 

## Bases de données
Théoriquement, ce plugin peut fonctionner avec n'importe quelle base de données du moment que les fichiers respectent la nomenclature "\<label\>_\<indice\>.png".
La base de données utilisée durant le développement comporte des chiffres manuscrits en 10 variations chacun, soit 100 images.

![database preview](https://github.com/Sydpy/OCR-ImageJ-plugin/blob/master/imgs/preview.png?raw=true "database preview")

On peut remarquer différentes manières d'écrire un même chiffre :
* Le 0 est un rond plus ou moins parfait, parfois une boucle
* Le 1 peut être une simple barre verticale, composé de deux traits ou même trois
* Le 2 contient parfois une petite boucle en bas à droite
* ...

Il est préférable d'avoir un maximum de variations d'un même caractère afin de trouver une ressemblance remarquable lors de l'étude d'un caractère. La base utilisée ne contient que 100 images mais couvre les principales variations de chaque caractère.

## Chaîne de traitements
L'image à étudier ainsi que toutes les images labellisées sont d'abord converties en niveaux de gris, binariser puis redimensionner en 50x50.
On calcule ensuite 4 caractéristiques pour chaque image qui vont composer son vecteur de caractéristiques :
* Le niveau de gris de chaque zone : l'image est découpée en neuf zones et pour chaque zone on calcule son niveau de gris moyen
* Le profil horizontal : nombre de pixels noirs par ligne
* Le profil vertical : nombre de pixels noirs par colonne
* Le rapport isopérimétrique : le périmètre du caractère

On cherche ensuite l'image labellisée dont le vecteur de caractéristiques a la plus petite distance Euclidienne avec celui de l'image à étudier.
On considère alors que l'image à étudier peut être labellisée par le label de cette image.

## Résultats
![confusion matrix](https://github.com/Sydpy/OCR-ImageJ-plugin/blob/master/imgs/confusion-matrix.png?raw=true "confusion matrix")
* 0 -> 90%, il a été confondu 1 fois avec le 9 sûrement à cause de leur courbure similaire.
* 1 -> 80%
* 2 -> 90%
* 3 -> 80%, il a pu être confondu avec le 7 à cause de la barre centrale de celui-ci
* 4 -> 90%
* 5 -> 100%
* 6 -> 100%
* 7 -> 70%
* 8 -> 80%, il a pu être confondu avec le 9 à cause de ces courbes similaires
* 9 -> 60%, a été confondu avec le 3 sûrement à cause de leur topologie très proche

Notre implémentation reconnaît les caractères à 84%, même si cela dépend beaucoup du caractère étudié comme vu précédemment. Pour améliorer ce taux, on pourrait implémenter de nouvelles caractéristiques à intégrer au vecteur de chaque image. Ces caractéristiques pourrait concerner la courbure et le nombre de traits par exemple.
