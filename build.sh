#!/usr/bin/env bash

JAR_FILE="OCR_.jar"
TMP_FILE="/tmp/ocr-imagej-plugin.tmp"
SRC_FOLDER="src/"
OUT_FOLDER="out/"
RES_FOLDER="res/"


if [ ! -d out/ ]; then
    mkdir $OUT_FOLDER
else
    rm -rf $OUT_FOLDER/*
fi


echo -n "Compiling sources... "

find $SRC_FOLDER -name \*.java -print > $TMP_FILE
javac -cp "lib/ij.jar" -d $OUT_FOLDER @$TMP_FILE

echo "Done"

echo -n "Generating .jar... "

if [ -f $JAR_FILE ]; then
    rm -rf $JAR_FILE
fi

cd $OUT_FOLDER
cp -r ../$RES_FOLDER/* .
find . > $TMP_FILE

jar cf $JAR_FILE @$TMP_FILE

cd ..

echo "Done"

echo -n "Cleaning... "


cp $OUT_FOLDER/$JAR_FILE .
cp $JAR_FILE ~/.imagej/plugins/
rm -rf $TMP_FILE $OUT_FOLDER

echo "Done"
